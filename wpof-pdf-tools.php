<?php
/*
 * wpof-pdf-tools.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@gabian-libre.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

// Inclure dompdf installé via composer
require_once 'vendor/autoload.php';
use Dompdf\Dompdf;
use Dompdf\Options;

define( 'wpof_url_pdf', WP_CONTENT_URL . "/uploads/pdf");
define( 'wpof_path_pdf', WP_CONTENT_DIR . "/uploads/pdf");
 
/*
 * Gestion des exports en PDF
 */
 
/*
 * Modèle de document pdf
 */
function pdf_from_model()
{
    $options = new Options();
    $options->set('isRemoteEnabled', TRUE);
    $options->set('isPhpEnabled', TRUE);
    
    $pdf_model = new Dompdf($options);
    
    return $pdf_model;
}


/*
 * Modèle de contenu HTML
 * Comme on ne peut invoquer plusieurs fois loadHtml, on prépare une chaîne html envoyée en une seule fois
 */
function html_from_model()
{
    $html = "";
    
    ob_start();?>
    <?php
    $html = ob_get_clean();
    
    $html .= "<style type='text/css'>".get_option("pdf-css")."</style>";
   /* if (debug && $_SERVER['SERVER_NAME'] == "make-formations.fr")
        $html .= "<div id='header'><img src='".WP_CONTENT_DIR . "/uploads/2018/06//Artefacts-logo-300x124.png' /></div>";
    else*/
        $html .= "<div id='header'>".wpautop(get_option("pdf-header"))."</div>";
    $html .= "<div id='footer'>".wpautop(get_option("pdf-footer"))."<div class='page-number'></div></div>";
    
    return $html;
}

/*
 * Création du dossier d'inscription
 */
function pdf_inscription($session_id, $user_id, $formation_id)
{
    $dompdf = pdf_from_model();
    $html = html_from_model();
    
    $ri = get_post(get_option('reglement-interieur-id'));
    $html .= "<h1>".$ri->post_title."</h1>";
    $html .= wpautop($ri->post_content);
    
    // output the HTML content
    $dompdf->loadHtml($html);
    
    //Close and output PDF document
    pdf_save($dompdf, "reglement-interieur", $session_id, $user_id);
}

/*
 * Création de la convention de formation professionnelle
 */
function pdf_convention($session_id, $user_id, $formation_id)
{
    $dompdf = pdf_from_model();
    $html = html_from_model();
    
    $convention = get_post(get_option('convention-id'));
    $html .= wpautop(substitute_values($convention->post_content, $session_id, $user_id, $formation_id));
    
    // output the HTML content
    $dompdf->loadHtml($html);
    
    //Close and output PDF document
    pdf_save($dompdf, "convention", $session_id, $user_id);
}


function parse_pre_inscription($session_id, $user_id, $formation_id)
{
    $sd = json_decode(get_user_meta($user_id, "session".$session_id, true), true);
}

function pdf_save($pdf, $name, $session_id, $user_id)
{
    $filepath = wpof_path_pdf ."/". date("Y/m");
    if (!is_dir($filepath)) mkdir($filepath, 0777, true);
    
    $filename = "$session_id-$user_id-$name.pdf";
    $pdf->render();
    $pdf_content = $pdf->output();
    
    $pdf_file = fopen($filepath."/".$filename, "w");
    fwrite($pdf_file, $pdf_content);
}


/*
 * Substitution des variables disséminées dans les textes de base
 */
function substitute_values($text, $session_id, $user_id, $formation_id)
{
    $session_stagiaire = json_decode(get_user_meta($user_id, "session".$session_id, true), true);
    
    //debug_info($session_stagiaire, "session_stagiaire");
    
    $formation = get_post($formation_id);
    
    $text = str_replace("{stagiaire}", get_user_meta($user_id, "first_name", true)." ".get_user_meta($user_id, "last_name", true), $text);
    if ($session_stagiaire['has-employeur'])
    {
        $entreprise = get_post($session_stagiaire['entreprise']);
        $text = str_replace("{entreprise}", $entreprise->post_title, $text);
        $text = str_replace("{client}", $entreprise->post_title, $text);
        $text = str_replace("{adresse}", get_field("adresse", $session_stagiaire['entreprise']), $text);
        $text = str_replace("{cp-ville}", get_field("cp_ville", $session_stagiaire['entreprise']), $text);
    }
    else
    {
        $text = str_replace("{client}", get_user_meta($user_id, "first_name", true)." ".get_user_meta($user_id, "last_name", true), $text);
        $text = str_replace("{entreprise}", "", $text);
        $text = str_replace("{adresse}", $session_stagiaire['adresse'], $text);
        $text = str_replace("{cp-ville}", $session_stagiaire['cp_ville'], $text);
    }
    
    $text = str_replace("{formation-titre}", $formation->post_title, $text);
    
    $pluriel = ($session_stagiaire['nb-jour'] > 1) ? "s" : "";
    $text = str_replace("{duree}", $session_stagiaire['nb-jour']." jour$pluriel (soit ".$session_stagiaire['nb-heure']." heures)", $text);
    $text = str_replace("{tarif-jour}", $session_stagiaire['tarif-jour']." ".get_option("monnaie-symbole"), $text);
    
    $text = str_replace("{dates}", pretty_print_dates(get_field("dates", $session_id)), $text);
    $text = str_replace("{pre-requis}", wpautop(get_field("pre-requis", $formation_id)), $text);
    
    $formateurs = get_field("formateur", $session_id);
    $liste_formateurs = array();
    foreach($formateurs as $f)
    {
        $liste_formateurs[] = $f['display_name'];
    }
    $text = str_replace("{formateur}", join(", ", $liste_formateurs), $text);
    
    $text = str_replace("{tarif-chiffre}", $session_stagiaire['tarif-total-chiffre'], $text);
    $text = str_replace("{monnaie-symbole}", get_option('monnaie-symbole') , $text);
    $text = str_replace("{tarif-lettre}", $session_stagiaire['tarif-total-lettre'], $text);
    $text = str_replace("{monnaie}", get_option('monnaie')."s" , $text);
    $text = str_replace("{today}", pretty_print_dates(date("d/m/Y", time())), $text);
    
    $lieu_id = get_post(get_field("lieu", $session_id));
    $text = str_replace("{lieu}", get_the_title($lieu_id)."<br />".get_field("adresse", $lieu_id)."<br />".get_field("code_postal", $lieu_id)." ".get_field("ville", $lieu_id), $text);
    $text = str_replace("{objectifs}", get_field("objectifs", $formation_id), $text);
    
    $programme_session = get_field("programme", $session_id);
    if (trim(strip_tags($programme_session)) == "") $programme_session = get_field("programme", $formation_id);
    $text = str_replace("{programme}", $programme_session, $text);
    $text = str_replace("{materiel_pedagogique}", get_field("materiel_pedagogique", $formation_id) , $text);
    $text = str_replace("{public}", get_field("public", get_field("public", $formation_id)), $text);
    /*
    $text = str_replace("{}", , $text);
    */
    
    return $text;
}
